package com.vitasoft.tech;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class SmaGateWayServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmaGateWayServerApplication.class, args);
	}

}
